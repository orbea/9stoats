# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit tmpfiles

if [[ ${PV} == 9999 ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/juur/${PN}.git"
else
	SRC_URI="https://github.com/juur/${PN}/archive/v${PV}/${P}.tar.gz"
	KEYWORDS="~amd64"
fi

DESCRIPTION="systemd-tmpfiles replacement; includes support for sysvinit style enviroments"
HOMEPAGE="https://github.com/juur/tmpfilesd"

LICENSE="MIT"
SLOT="0"

BDEPEND="
	app-shells/bash
	sys-apps/which
	virtual/pkgconfig
"
RDEPEND="
	!sys-apps/systemd-utils[tmpfiles]
"

src_configure()  {
	econf --bindir="${EPREFIX}"/bin
}

src_install() {
	emake DESTDIR="${ED}" install
	einstalldocs
	keepdir /etc/tmpfiles.d
	exeinto /etc/cron.daily
	doexe "${FILESDIR}"/tmpfilesd-clean
	cd misc || die
	for f in tmpfilesd-dev tmpfilesd-setup; do
		newconfd tmpfilesd.sysconfig ${f}
		newinitd "${FILESDIR}/${f}.initd" ${f}
	done
}

add_service() {
	local initd=$1
	local runlevel=$2

	elog "Auto-adding '${initd}' service to your ${runlevel} runlevel"
	mkdir -p "${EROOT}"/etc/runlevels/${runlevel} || die
	ln -snf /etc/init.d/${initd} "${EROOT}"/etc/runlevels/${runlevel}/${initd} || die
}

pkg_postinst() {
	tmpfiles_process tmp.conf
	tmpfiles_process var.conf
	if [[ -z $REPLACING_VERSIONS ]]; then
		add_service tmpfilesd-dev sysinit
		add_service tmpfilesd-setup boot
	fi
}
