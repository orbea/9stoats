Gentoo Ebuilds.

	To add the overlay.
	eselect repository add 9stoats git https://0xacab.org/orbea/9stoats.git

	Or with layman.
	layman -o https://0xacab.org/orbea/9stoats/raw/master/repo.xml -f -a 9stoats
