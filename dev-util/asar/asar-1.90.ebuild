# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

CMAKE_USE_DIR="${S}/src"

if [[ ${PV} == 9999 ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/RPGHacker/${PN}.git"
else
	SRC_URI="https://github.com/RPGHacker/${PN}/archive/v${PV}/${P}.tar.gz"
	KEYWORDS="~amd64"
fi

DESCRIPTION="SNES Assembler"
HOMEPAGE="https://github.com/RPGHacker/asar"

LICENSE="LGPL-3+"
SLOT="0"
IUSE="shared test"
RESTRICT="!test? ( test )"

PATCHES=(
	"${FILESDIR}"/${P}-cmake-warning.patch
	"${FILESDIR}"/${P}-format-truncation.patch
	"${FILESDIR}"/${P}-lib-version.patch
	"${FILESDIR}"/${P}-misleading-indents.patch
)

src_configure() {
	local mycmakeargs=(
		-DASAR_GEN_DLL=$(usex shared) # shared
		-DASAR_GEN_LIB=OFF # static
		-DASAR_GEN_EXE_TEST=$(usex test)
	)

	if use shared; then
		mycmakeargs+=( -DASAR_GEN_DLL_TEST=$(usex test) )
	else
		mycmakeargs+=( -DASAR_GEN_DLL_TEST=OFF )
	fi

	cmake_src_configure
}

src_install() {
	cmake_src_install

	dodoc README.md README.txt
}

src_test () {
	local tests=( run-app-test )
	use shared && tests+=( run-dll-test )

	cmake_build "${tests[@]}"
}
